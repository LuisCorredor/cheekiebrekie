<!Doctype html>
<html>

<head>
    <?php include 'includes/header.php';?>
</head>

<body class="fondo">
    <div class="container">
        <div class="col-md-12 card-space">
            <div class="card">
                <div class="card-body">
                    <center>
                        <img src="img/music-player.png" class="logo">
                    </center>

                    <h5 class="card-title" align="center">Cheekiebrekie</h5>

                    <form action="process.php" method="post" autocomplete="off" class="col-md-12">
                        <div class="form-group">
                            <label for="nombre">Username:</label>
                            <input id="nombre" type="text" class="form-control" name="nombre" placeholder="Ingresa tu nombre de usuario" required>
                        </div>

                        <div class="form-group">
                            <label for="password">Password:</label>
                            <input type="password" class="form-control" name="password" placeholder="Ingresa tu contraseña" required>
                        </div>

                        <div class="input-group">
                            <button type="submit" class="btn btn-block btn-primary" name="login_user">Ingresar</button>
                        </div>
                        
                        <div class="input-group">
                            <a class="btn btn-block" href="userregistro.php">Registrar</a>
                        </div>

                        <div class="input-group">
                            <a class="btn btn-block" href="admin.php" style="margin-top:10px;">Admin</a>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

    <?php include 'includes/footer.php';?>
</body>

</html>
