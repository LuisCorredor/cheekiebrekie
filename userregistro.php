<html>

<head>
    <?php include "includes/header.php";?>
</head>

<body>
    <div class="container">
        <div class="row space">
            <div class="col-md-6">
                <h2>Registro de usuarios</h2>

                <form role="form" method="post" action="php/registrarusuario.php">
                    <div class="form-group">
                        <label for="nombre">Nombre</label>
                        <input type="text" class="form-control" name="nombre" required>
                    </div>
                    <div class="form-group">
                        <label for="password">Password</label>
                        <input type="password" class="form-control" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" name="email" required>
                    </div>

                    <button type="submit" class="btn btn-default">Registrar</button>
                    <a href="index.php" class="btn">Regresar</a>
                </form>
            </div>
        </div>
    </div>

    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>
