<!Doctype html>
<html>

<head>
    <?php include 'includes/header.php';?>
</head>

<body>
    <?php include 'includes/nav.php';?>

    <div class="container">
        <div class="row space">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Bievenido a Cheekiebrekie</h5>
                        <p class="card-text">La aplicación para cargar la última información en Música.</p>
                    </div>
                </div>
            </div>

            <div class="col-12" style="margin-top:20px;">
                <div class="row">

                    <div class="col col-sm-6">
                        <a href="verbanda.php">
                            <div class="card">
                                <div class="card-body">
                                    <center>
                                        <img src="img/imagenn%202.jpg" class="icon">
                                    </center>
                                    <h5 class="card-title title">Registrar Banda</h5>
                                </div>
                            </div>
                        </a>
                    </div>

                    <div class="col col-sm-6">
                        <a href="vercancion.php">
                            <div class="card">
                                <div class="card-body">
                                    <center>
                                        <img src="img/Power%20Under%20Control.%20imagen%201.jpg" class="icon">
                                    </center>
                                    <h5 class="card-title title">Registrar Canción</h5>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php include 'includes/footer.php';?>
</body>

</html>
