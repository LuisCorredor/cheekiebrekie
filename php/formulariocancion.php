<?php
    include "conexion.php";

    $sql1= "SELECT * FROM cancion WHERE idCancion = ".$_GET["id"];
    $query = $con->query($sql1);
    $cancion = null;
    if($query->num_rows>0){
        while ($r=$query->fetch_object()){
            $cancion=$r;
        break;
        }
    }
?>

    <?php if($cancion!=null):?>

    <form role="form" method="post" action="php/actualizarCancion.php">
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" value="<?php echo $cancion->nombre; ?>" name="nombre" required>
        </div>
        <div class="form-group">
            <label for="duracion">Duracion</label>
            <input type="text" class="form-control" value="<?php echo $cancion->duracion; ?>" name="duracion" required>
        </div>
        <div class="form-group">
            <label for="genero">Id Banda</label>
            <input type="text" class="form-control" value="<?php echo $cancion->idBanda; ?>" name="idBanda" required>
        </div>
      
        <input type="hidden" name="id" value="<?php echo $cancion->idCancion; ?>">
        <button type="submit" class="btn btn-default">Actualizar</button>
    </form>
    <?php else:?>
        <p class="alert alert-danger">404 No se encuentra</p>
    <?php endif;?>
