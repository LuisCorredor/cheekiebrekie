<?php

include "conexion.php";

    $sql1= "select * from banda";
    $query = $con->query($sql1);
?>

    <?php if($query->num_rows>0):?>
    <table class="table table-bordered table-hover">
        <thead>
            <th>Id Banda</th>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Genero</th>
            <th>Cantidad de Integrantes</th>

        </thead>
        <?php while ($r=$query->fetch_array()):?>
        <tr>
            <td>
                <?php echo $r["idBanda"]; ?>
            </td>
            <td>
                <?php echo $r["nombreBanda"]; ?>
            </td>
            <td>
                <?php echo $r["descripcion"]; ?>
            </td>
            <td>
                <?php echo $r["genero"]; ?>
            </td>
            <td>
                <?php echo $r["cantidadIntegrantes"]; ?> Integrantes
            </td>

            <td style="width:200px;">
                <a href="./editar.php?id=<?php echo $r["idBanda"];?>" class="btn btn-sm btn-warning">Editar</a>
                <a href="php/eliminar.php?id=<?php echo $r["idBanda"];?>" class="btn btn-sm btn-danger">Eliminar</a>
            </td>
        </tr>
        <?php endwhile;?>
    </table>
    <?php else:?>
    <p class="alert alert-warning">No hay partidos cargados</p>
    <?php endif;?>
