<br><br>
<h4 align="center">Reproducir todo</h4>
<br>

<?php

    include "conexion.php";

        $sql1= "select * from cancion";
        $query = $con->query($sql1);
    ?>

    <?php if($query->num_rows>0):?>
    <center>
        <audio id="audio" preload="auto" tabindex="0" controls=""></audio>


        <ul id="playlist" align="center">
            <!-- Demo de reproducción automática -->
            <li>
                <a href="https://s3-us-west-2.amazonaws.com/allmetalmixtapes/Saxon%20-%201984%20-%20Crusader/01%20-%20Crusader%20Prelude.mp3">
                Cancion 1
            </a>
            </li>
            <li>
                <a href="https://s3-us-west-2.amazonaws.com/allmetalmixtapes/Saxon%20-%201984%20-%20Crusader/03%20-%20Little%20Bit%20Of%20What%20You%20Fancy.mp3">
                Cancion 2
            </a>
            </li>

            <!-- Fin demo reproducción toma en el caso de una consulta local la primera pista -->

            <?php while ($pista=$query->fetch_array()):?>
            <li>
                <a href="php/<?php echo $pista["rutaCancion"]; ?>">
                    <?php echo $pista["nombre"]; ?>
                </a>
            </li>

            <!-- Luego de tomar la primera, hace una petición en dondes espera una estructura http estático y la estructura de la url se enlaza dinámico lo que genera que en algunos entornos locales el reproductor automático deje de reproducir -->

            <?php endwhile;?>
        </ul>
    </center>

    <?php else:?>
    <p class="alert alert-warning">No hay canciones cargadas</p>
    <?php endif;?>
