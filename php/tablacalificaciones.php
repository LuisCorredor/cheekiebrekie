<?php

include "conexion.php";

    $sql1= "select * from calificaciones";
    $query = $con->query($sql1);
?>

    <?php if($query->num_rows>0):?>
    <table class="table table-bordered table-hover">
        <thead>
            <th>Id Calificación</th>
            <th>Texto</th>
            <th>Id Banda</th>

        </thead>
        <?php while ($r=$query->fetch_array()):?>
        <tr>
            <td>
                <?php echo $r["idCalificacion"]; ?>
            </td>
            <td>
                <?php echo $r["texto"]; ?>
            </td>
            <td>
                <?php echo $r["idBanda"]; ?>
            </td>
       

            <td style="width:200px;">
                <a href="./editarcalificacion.php?id=<?php echo $r["idCalificacion"];?>" class="btn btn-sm btn-warning">Editar</a>
            </td>
        </tr>
        <?php endwhile;?>
    </table>
    <?php else:?>
    <p class="alert alert-warning">No hay calificaciones cargadas</p>
    <?php endif;?>
