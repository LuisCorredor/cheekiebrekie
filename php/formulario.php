<?php
    include "conexion.php";

    $sql1= "SELECT * FROM Banda WHERE idBanda = ".$_GET["id"];
    $query = $con->query($sql1);
    $banda = null;
    if($query->num_rows>0){
        while ($r=$query->fetch_object()){
            $banda=$r;
        break;
        }
    }
?>

    <?php if($banda!=null):?>

    <form role="form" method="post" action="php/actualizar.php">
        <div class="form-group">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" value="<?php echo $banda->nombreBanda; ?>" name="nombreBanda" required>
        </div>
        <div class="form-group">
            <label for="descripcion">Descripcion</label>
            <input type="text" class="form-control" value="<?php echo $banda->descripcion; ?>" name="descripcion" required>
        </div>
        <div class="form-group">
            <label for="genero">Genero</label>
            <input type="text" class="form-control" value="<?php echo $banda->genero; ?>" name="genero" required>
        </div>
        <div class="form-group">
            <label for="cantidadIntegrantes">Cantidad de Integrantes</label>
            <input type="text" class="form-control" value="<?php echo $banda->cantidadIntegrantes; ?>" name="cantidadIntegrantes" required>
        </div>
      
        <input type="hidden" name="id" value="<?php echo $banda->idBanda; ?>">
        <button type="submit" class="btn btn-default">Actualizar</button>
    </form>
    <?php else:?>
        <p class="alert alert-danger">404 No se encuentra</p>
    <?php endif;?>
