<?php

include "conexion.php";

    $sql1= "select * from cancion";
    $query = $con->query($sql1);
?>

    <?php if($query->num_rows>0):?>
    <table class="table table-bordered table-hover">
        <thead>
            <th>Id cancion</th>
            <th>Nombre</th>
            <th>Duracion</th>
            <th>Cancion</th>
            <th>Id Banda</th>

        </thead>
        
        <!-- Demo reproductor lista 3 y 4 pasan sin problema por la composición de la URL -->
        <tr>
            <td>3</td>
            <td>Cancion 1</td>
            <td>1:07 minutos</td>
            <td><audio src="https://s3-us-west-2.amazonaws.com/allmetalmixtapes/Saxon%20-%201984%20-%20Crusader/01%20-%20Crusader%20Prelude.mp3" controls="controls" type="audio/mpeg" preload="preload"></audio></td>
            <td>
                1
            </td>
            <td><a href="./editarCancion.php?id=<?php echo $r["idCancion"];?>" class="btn btn-sm btn-warning">Editar</a>
                <a href="php/eliminarCancion.php?id=<?php echo $r["idCancion"];?>" id="del-<?php echo $r["idCancion"];?>" class="btn btn-sm btn-danger">Eliminar</a></td>
        </tr>
        
        <tr>
            <td>4</td>
            <td>Cancion 2</td>
            <td>3:52 minutos</td>
            <td><audio src="https://s3-us-west-2.amazonaws.com/allmetalmixtapes/Saxon%20-%201984%20-%20Crusader/03%20-%20Little%20Bit%20Of%20What%20You%20Fancy.mp3" controls="controls" type="audio/mpeg" preload="preload"></audio></td>
            <td>
                5
            </td>
            <td><a href="./editarCancion.php?id=<?php echo $r["idCancion"];?>" class="btn btn-sm btn-warning">Editar</a>
                <a href="php/eliminarCancion.php?id=<?php echo $r["idCancion"];?>" id="del-<?php echo $r["idCancion"];?>" class="btn btn-sm btn-danger">Eliminar</a></td>
        </tr>
        
        <!-- Composición de URL desde un servidor -->
        
        <?php while ($r=$query->fetch_array()):?>
        <tr>
            <td>
                <?php echo $r["idCancion"]; ?>
            </td>
            <td>
                <?php echo $r["nombre"]; ?>
            </td>
            <td>
                <?php echo $r["duracion"]; ?>
            </td>
            <td>
                <audio src="php/<?php echo $r["rutaCancion"]; ?>" controls="controls" type="audio/mpeg" preload="preload"></audio>
            </td>
            <td>
                <?php echo $r["idBanda"]; ?>
            </td>

            <td style="width:200px;">
                <a href="./editarCancion.php?id=<?php echo $r["idCancion"];?>" class="btn btn-sm btn-warning">Editar</a>
                <a href="php/eliminarCancion.php?id=<?php echo $r["idCancion"];?>" id="del-<?php echo $r["idCancion"];?>" class="btn btn-sm btn-danger">Eliminar</a>
            </td>
        </tr>
        <?php endwhile;?>
    </table>

    <?php else:?>
    <p class="alert alert-warning">No hay canciones cargadas</p>
    <?php endif;?>