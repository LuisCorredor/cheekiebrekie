<title>Cheekiebrekie</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

<!-- styles css -->
<link href="css/style.css" rel="stylesheet" />

<!-- google fonts -->
<link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">

<!-- metas -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

