<html>
    
<head>
    <?php include 'includes/header.php';?>
</head>

<body>
    <?php include 'includes/nav.php';?>

    <div class="container">
        <div class="row">
            <div class="col-md-12 space">
                <h2>Canciones Registradas</h2>
                <a data-toggle="modal" href="#myModal" class="btn btn-primary">Agregar cancion</a>
                <br><br>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Agregar cancion</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body">
                                <form role="form" method="post" action="php/agregarcancion.php" enctype="multipart/form-data">
                                    <div class="form-group">
                                        <label for="direccion">Nombre</label>
                                        <input type="text" class="form-control" name="nombre" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="direccion">Duracion</label>
                                        <input type="text" class="form-control" name="duracion" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="idBanda">Id Banda</label>
                                        <input type="text" class="form-control" name="idBanda" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="rutaCancion">Archivo de canción</label>
                                        <input type="file" class="form-control" name="archivo" accept=".mp3,audio/*" required>
                                    </div>

                                    <button type="submit" class="btn btn-default">Agregar</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <?php include "php/tablacanciones.php"; ?>

                <?php include "php/reproducirtodo.php"; ?>

            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <script>
        init();

        function init() {
            var audio = document.getElementById('audio');
            var playlist = document.getElementById('playlist');
            var tracks = playlist.getElementsByTagName('a');
            audio.volume = 0.10;
            audio.play();


            //Agregamos los eventos a los links que nos permitirán cambiar de canción
            for (var track in tracks) {
                var link = tracks[track];
                if (typeof link === "function" || typeof link === "number") continue;
                link.addEventListener('click', function(e) {
                    e.preventDefault();
                    var song = this.getAttribute('href');
                    run(song, audio, this);
                });
            }

            //agregamos evento para reproducir la siguiente canción en la lista
            //si la canción es la ultima reproducir la primera otra vez
            audio.addEventListener('ended', function(e) {
                for (var track in tracks) {
                    var link = tracks[track];
                    var nextTrack = parseInt(track) + 1;
                    if (typeof link === "function" || typeof link === "number") continue;
                    if (!this.src) this.src = tracks[0];
                    if (track == (tracks.length - 1)) nextTrack = 0;
                    console.log(nextTrack);
                    if (link.getAttribute('href') === this.src) {
                        var nextLink = tracks[nextTrack];
                        run(nextLink.getAttribute('href'), audio, nextLink);
                        break;
                    }
                }
            });
        }

        function run(song, audio, link) {
            var parent = link.parentElement;
            //quitar el active de todos los elementos de la lista
            var items = parent.parentElement.getElementsByTagName('li');
            for (var item in items) {
                if (items[item].classList)
                    items[item].classList.remove("active");
            }

            //agregar active a este elemento
            parent.classList.add("active");

            //tocar la cancion
            audio.src = song;
            audio.load();
            audio.play();
        }

    </script>
</body>

</html>
