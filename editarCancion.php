<html>

<head>
    <?php include "includes/header.php";?>
</head>

<body>
    <?php include "includes/nav.php"; ?>
    <div class="container">
        <div class="row space">
            <div class="col-md-6">
                <h2>Editar datos de la cancion</h2>

                <?php include "php/formulariocancion.php";?>

            </div>
        </div>
    </div>

    <script src="bootstrap/js/bootstrap.min.js"></script>
</body>

</html>