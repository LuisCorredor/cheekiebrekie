<html>

<head>
    <?php include 'includes/header.php';?>
</head>

<body>
    <?php include 'includes/nav.php';?>

    <div class="container">
        <div class="row">
            <div class="col-md-12 space">
                <h2>Bandas Registradas</h2>
                <a data-toggle="modal" href="#myModal" class="btn btn-primary">Agregar banda</a>
                <br><br>

                <!-- Modal -->
                <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Agregar banda</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>

                            <div class="modal-body">
                                <form role="form" method="post" action="php/agregar.php">
                                    <div class="form-group">
                                        <label for="nombre">Nombre</label>
                                        <input type="text" class="form-control" name="nombreBanda" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="direccion">Descripcion</label>
                                        <input type="text" class="form-control" name="descripcion" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="cantidad">Genero</label>
                                        <input type="text" class="form-control" name="genero" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="tipo">Cantidad de Integrantes</label>
                                        <input type="text" class="form-control" name="cantidadIntegrantes" required>
                                    </div>

                                    <button type="submit" class="btn btn-default">Agregar</button>
                                </form>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                    <!-- /.modal-dialog -->
                </div>
                <!-- /.modal -->

                <?php include "php/tabla.php"; ?>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</body>

</html>
